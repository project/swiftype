<?php

/**
 * @file
 * Swiftype integration module main file.
 */

/**
 * Implements hook_help().
 */
function swiftype_integration_help($path, $arg) {
  switch ($path) {
    case 'admin/help#swiftype_integration':

      $filepath = dirname(__FILE__) . '/README.md';
      if (file_exists($filepath)) {
        $readme = file_get_contents($filepath);
      }
      else {
        $filepath = dirname(__FILE__) . '/README.txt';
        if (file_exists($filepath)) {
          $readme = file_get_contents($filepath);
        }
      }
      if (!isset($readme)) {
        return NULL;
      }
      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];

        if (function_exists($info['process callback'])) {
          $output = $info['process callback']($readme, NULL);
        }
        else {
          $output = '<pre>' . $readme . '</pre>';
        }
      }
      else {
        $output = '<pre>' . $readme . '</pre>';
      }

      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function swiftype_integration_menu() {
  $items = array();

  $items['admin/config/search/swiftype'] = array(
    'title' => 'Swiftype integration settings',
    'description' => 'Adjust Swiftype integration settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('swiftype_integration_configure_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/swiftype.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_block_info().
 */
function swiftype_integration_block_info() {
  $blocks['swiftype_integration'] = array(
    'info' => t('Swiftype search'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function swiftype_integration_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'swiftype_integration':
      // Set module title.
      $block['subject'] = t('Swiftype search');
      $block['content'] = array(
        // Add search field with Swiftype pre-defined class.
        '#markup' => '<input type="text" class="st-default-search-input">',
        '#attached' => array(
          'js' => array(
            // Add JS file with Swiftype install code.
            drupal_get_path('module', 'swiftype_integration') . '/includes/swiftype.js',
            // Pass Swiftype install key to JS file with Swiftype install code.
            array(
              'data' => array('swiftype_integration_install_key' => variable_get('swiftype_integration_install_key')),
              'type' => 'setting',
            ),
          ),
        ),
      );
      break;
  }

  return $block;
}
